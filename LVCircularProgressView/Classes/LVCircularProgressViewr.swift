//
//  DVCircularSlider.swift
//  
//
//  Created by Darth Vader on 8/9/16.
//
//

import UIKit
import CoreGraphics


@IBDesignable public class LVCircularProgressView : UIControl
{
    var radius : CGFloat = 0
    @IBInspectable var innerRadius : CGFloat = 0
    private var angle : Int = 90
    @IBInspectable var startColor : UIColor = UIColor.white
    @IBInspectable var endColor : UIColor = UIColor.gray
    @IBInspectable var sliderBackgroundColor : UIColor = UIColor.black
    @IBInspectable var lineWidth : CGFloat = 40
    @IBInspectable var blur : Bool = false
    @IBInspectable var blurAmount : Double = 0
    @IBInspectable var blurAmountToValueRatio : Double = 0
    
    @IBInspectable var handle : Bool = false
    @IBInspectable var handleBlur : Bool = false
    @IBInspectable var handleBlurAmount : CGFloat = 0
    @IBInspectable var handleBlurToValueRatio : CGFloat = 0
    @IBInspectable var handleColor : UIColor = UIColor.white
    @IBInspectable var handleWidth : CGFloat = 40
    @IBInspectable var handleWidthToLineWidthRatio : CGFloat = 1
    @IBInspectable var clockWise : Bool = true
    
    @IBInspectable var startAngle : Double = 0

    @IBInspectable var endAngle : Double = 360
  
    
    @IBInspectable var minValue : Double = 0
    @IBInspectable var maxValue : Double = 100
    @IBInspectable var value : Double = 90


    override public func prepareForInterfaceBuilder()
    {
        setNeedsDisplay()
    }
    
    required public init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
    }

    public func setProgress(_ progress:Double) {
        value = progress
        setNeedsDisplay()
    }

    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        var maskStartAngle : Double = 0
        
        maskStartAngle = clockWise ? startAngle : endAngle
        let end = endAngle < startAngle ? endAngle + 360 : endAngle
        let valueRatio = ( value - minValue ) / (maxValue - minValue)
        
        let base = clockWise ? startAngle : endAngle
        let k : Double = clockWise ? 1 : -1
        
        let val = Int( floor( valueRatio * abs(end - startAngle) * k + base ) )
        angle = val

        

        radius = innerRadius != 0 ? innerRadius + lineWidth/2 : self.frame.width / 2 - lineWidth/2
        let mainContext = UIGraphicsGetCurrentContext()
        
        /* Background */
        
        let startAngleBackground = DegreesToRadians(value: startAngle)
        let maxAngleBackground = DegreesToRadians(value: endAngle)

        mainContext?.addArc(center: CGPoint(x:self.bounds.midX,y:self.bounds.midY) , radius: radius, startAngle: CGFloat(startAngleBackground), endAngle: CGFloat(maxAngleBackground), clockwise: false)

        mainContext?.setLineWidth(lineWidth)
        mainContext?.setLineCap(.butt)

        sliderBackgroundColor.setStroke()
        mainContext?.drawPath(using: .stroke)

        
        /** Draw the actual slider arc **/
        
        /* Create a mask image context to clip gradient rect unerneath */
        
        let currentAngleMask = DegreesToRadians(value: Double(angle))
        let startAngleMask = DegreesToRadians(value: Double(maskStartAngle))
        
        UIGraphicsBeginImageContext(self.frame.size)
        let maskImageContext = UIGraphicsGetCurrentContext()



        maskImageContext?.translateBy(x: 0, y: frame.size.height)
        maskImageContext?.scaleBy(x: 1.0, y: -1.0)


        maskImageContext?.addArc(center: CGPoint(x:self.bounds.midX,y:self.bounds.midY), radius: radius, startAngle: CGFloat(startAngleMask), endAngle: CGFloat(currentAngleMask), clockwise: !clockWise)


        maskImageContext?.setStrokeColor(UIColor.brown.cgColor)

        
        
        /* Add shadow to create a blur effect */
        if (blur)
        {
            let blurAmountForMask = blurAmount != 0 ? blurAmount : Double(value) * blurAmountToValueRatio
            maskImageContext?.setShadow(offset: .zero, blur: CGFloat(blurAmountForMask), color: UIColor.brown.cgColor)
        }
        
        maskImageContext!.setLineWidth(lineWidth)
        maskImageContext?.drawPath(using: .stroke)
        
        let maskImage = maskImageContext!.makeImage()
        UIGraphicsEndImageContext()
        
        /* Clip context to the maskImage */
        mainContext!.saveGState()
        mainContext!.clip(to: self.bounds, mask: maskImage!)
        
        /*Gradient fill*/
        
        /* Split colors in components */

        let startColorComponents = startColor.cgColor.components!
        let endColorComponents = endColor.cgColor.converted(to: CGColorSpaceCreateDeviceRGB(), intent: .defaultIntent, options: nil)!.components!
        
        //TODO:FIND HOW TO ENUMERATE THIS SHIT
        
        let colorComponents = [ startColorComponents[0], startColorComponents[1], startColorComponents[2], 1.0 ,
                                endColorComponents[0], endColorComponents[1], endColorComponents[2], 1.0 ]
        
        /* Gradient Setup */
        let gradient = CGGradient(colorSpace: CGColorSpaceCreateDeviceRGB(),
                                  colorComponents: colorComponents,
                                  locations: nil,
                                  count: 2)
        /* Gradient direction */
        let startPoint = CGPoint(x:rect.midX,y:rect.minY)
        let endPoint = CGPoint(x:rect.midX, y:rect.maxY)
        
        /* drawGradient */
        mainContext?.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: [])
        
        /*draw handle if needed*/
        if(handle)
        {
            drawHandle(context: mainContext!)
        }
    }
    
    func drawHandle(context:CGContext)
    {
        context.saveGState()
        
        if ( handleBlur )
        {
            let blurAmountForHandle = handleBlurToValueRatio != 0 ? handleBlurToValueRatio * CGFloat(angle) : handleBlurAmount
            context.setShadow(offset: .zero, blur: blurAmountForHandle, color: UIColor.black.cgColor)
        }
        
        let handleCenter = pointFromAngle(angleInt: angle)
        
        handleColor.setFill()
        let widthOfHandle = handleWidthToLineWidthRatio != 0 ? handleWidthToLineWidthRatio * lineWidth : handleWidth
        context.fillEllipse(in: CGRect(x:handleCenter.x, y:handleCenter.y,width: widthOfHandle,height: widthOfHandle ))
        context.restoreGState()
    }

    /** Given the angle, get the point position on circumference **/
    func pointFromAngle(angleInt:Int)->CGPoint
    {
        //Circle center
        let centerPoint = CGPoint(x:self.frame.size.width/2.0 - lineWidth/2.0,y: self.frame.size.height/2.0 - lineWidth/2.0)

        //The point position on the circumference
        var result:CGPoint = CGPoint.zero
        let y = round(Double(radius) * sin(DegreesToRadians(value:Double(-angleInt)))) + Double(centerPoint.y)
        let x = round(Double(radius) * cos(DegreesToRadians(value:Double(-angleInt)))) + Double(centerPoint.x)
        result.y = CGFloat(y)
        result.x = CGFloat(x)

        return result
    }
}

// MARK: Math Helpers

func DegreesToRadians (value:Double) -> Double {
    return value * .pi / 180.0
}

func RadiansToDegrees (value:Double) -> Double {
    return value * 180.0 / .pi
}

func Square (value:CGFloat) -> CGFloat {
    return value * value
}




















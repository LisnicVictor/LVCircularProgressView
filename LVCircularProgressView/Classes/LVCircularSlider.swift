//
//  LVCircularSliderr.swift
//  LVCircularProgressView
//
//  Created by Victor Lisnic on 10/16/17.
//

import UIKit
import CoreGraphics


@IBDesignable public class LVCircularSlider : LVCircularProgressView {

    override public func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        return true
    }

    override public func continueTracking(_ touch: UITouch, with  event: UIEvent?) -> Bool {
        super.continueTracking(touch, with: event)

        let lastPoint = touch.location(in: self)

        changeValue(point: lastPoint)

        sendActions(for: UIControlEvents.valueChanged)
        print(value)
        return true
    }

    override public func endTracking(_ touch: UITouch?, with  event: UIEvent?) {
        super.endTracking(touch, with: event)
    }

    func changeValue(point:CGPoint) {
        //Calculate the direction from a center point and a arbitrary position.
        let center = CGPoint.init(x: self.bounds.midX, y: self.bounds.midY)
        let currentAngle:Double = AngleFromNorth(p1: center, p2: point, flipped: true);
        var angleInt = Int(floor(currentAngle))

        let end = endAngle < startAngle ? endAngle + 360 : endAngle
        angleInt = Double(angleInt) < startAngle ? angleInt + 360 : angleInt

        let k : Double = clockWise ? 1 : -1
        let val : Double = clockWise ? minValue : maxValue

        let angleInZone : Bool = clockWise ?
            ( end >= Double(angleInt - 1) && Double(startAngle) <= Double(angleInt + 1) ) :
            ( Double(startAngle) <= Double(angleInt - 1) && end >= Double(angleInt + 1) )

        print(angleInt)

//        Store the new value
        if angleInZone {
            let progress = val + k * ( (maxValue - minValue) * abs( Double( Double(angleInt) - startAngle ) / (end - startAngle)))
            setProgress(progress)
        }
    }

    //Sourcecode from Apple example clockControl
    //Calculate the direction in degrees from a center point to an arbitrary position.
    func AngleFromNorth(p1:CGPoint , p2:CGPoint , flipped:Bool) -> Double {
        var v:CGPoint  = CGPoint(x:p2.x - p1.x, y:p2.y - p1.y)
        var vmag:CGFloat = Square(value: Square(value:v.x) + Square(value:v.y))
        var result:Double = 0.0
        vmag = vmag == 0 ? 0.001 : vmag
        v.x /= vmag;
        v.y /= vmag;
        let radians = Double(atan2(v.y,v.x))
        result = RadiansToDegrees(value:radians)
        return (result >= 0  ? result : result + 360.0);
    }
}

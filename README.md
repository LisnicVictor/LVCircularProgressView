# LVCircularProgressView

[![CI Status](http://img.shields.io/travis/LisnicVictor/LVCircularProgressView.svg?style=flat)](https://travis-ci.org/LisnicVictor/LVCircularProgressView)
[![Version](https://img.shields.io/cocoapods/v/LVCircularProgressView.svg?style=flat)](http://cocoapods.org/pods/LVCircularProgressView)
[![License](https://img.shields.io/cocoapods/l/LVCircularProgressView.svg?style=flat)](http://cocoapods.org/pods/LVCircularProgressView)
[![Platform](https://img.shields.io/cocoapods/p/LVCircularProgressView.svg?style=flat)](http://cocoapods.org/pods/LVCircularProgressView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LVCircularProgressView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LVCircularProgressView'
```

## Author

LisnicVictor, gerasim.kisslin@gmail.com

## License

LVCircularProgressView is available under the MIT license. See the LICENSE file for more info.
